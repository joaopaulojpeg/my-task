import Link from "next/link"
const Button = ({children, onClick, disabled, size = "w-full" , color = "bg-gray-800", textSize = "text-lg", hoverColor = "bg-gray-700" }) => (
  <>
    <button
      onClick={onClick}
      disabled={disabled}
      className={`${size} p-3 ${color} rounded-sm ${textSize}
                  font-medium text-white uppercase focus:outline-none 
                  hover:${hoverColor} hover:shadow-none`}>
      {children}
    </button>
  </>
);
export const ButtonLink = ({children, link, onClick, disabled, size = "w-full" , color = "bg-gray-800", textSize = "text-lg", hoverColor = "bg-gray-700" }) => (
  <Link href={link}>
    <button
      disabled={disabled}
      className={`${size} p-3 ${color} rounded-sm ${textSize}
                  font-medium text-white uppercase focus:outline-none 
                  hover:${hoverColor} hover:shadow-none`}>
      {children}
    </button>
  </Link>
);




export default Button;



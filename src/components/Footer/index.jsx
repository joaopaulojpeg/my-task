
 function toggleFooterSection(e) {
  const list = e.target.parentElement.parentElement.querySelector(
    "article"
  );
  if (list.classList.contains("h-0")) {
    list.classList.remove("h-0");
  } else {
    list.classList.add("h-0");
  }
}

const Footer = ({props}) => (
  <footer className="mt-16 bg-white bottom-0 w-full">
      <div className="border-t md:px-4 md:pt-10 md:pb-5">
        <div className="flex flex-wrap md:max-w-screen-lg mx-auto">
          <section
            className="relative text-gray-700 font-light font-light border-b px-6 pb-4 md:py-3 w-full md:border-none md:w-1/4"
          >
            <div className="md:hidden">
              <button
                onClick={() => toggleFooterSection(event)}
                className="uppercase text-xs font-bold tracking-wider focus:outline-none border-t border-white py-4 w-full text-left"
                type="button"
              >
                Empresa
              </button>
            </div>
            <a
              className="uppercase text-xs border-l-4 border-blue-500 p-2  font-bold tracking-wider hidden md:block"
              href="#"
            >
              Empresa
            </a>
            <article className="h-0 md:h-auto -mt-4 md:mt-0 overflow-hidden">
              <ul className="my-5 text-sm tracking-wide">
                <li className="my-3 tracking-wide">
                  <a href="https://www.ibm.com/br-pt/about?lnk=fab">Sobre nós</a>
                </li>
                <li className="my-3 tracking-wide">
                  <a href="https://www.ibm.com/ibm/responsibility/br-pt/?lnk=fab">Responsabilidade Corporativa</a>
                </li>
                <li className="my-3 tracking-wide">
                  <a href="https://www.ibm.com/jobs/br?lnk=fab">Carreiras</a>
                </li>
                <li className="my-3 tracking-wide">
                  <a href="https://www.ibm.com/support/br/pt/?lnk=fcw_brpt"> Entre em contato conosco</a>
                </li>
              </ul>
            </article>
          </section>

          <section
            className="relative text-gray-700 font-light font-light border-b px-6 pb-4 md:py-3 w-full md:border-none md:w-1/4"
          >
            <div className="md:hidden">
              <button
                   onClick={() => toggleFooterSection(event)}
                className="uppercase text-xs border-blue-500 font-bold tracking-wider focus:outline-none border-t border-white py-4 w-full text-left"
                type="button"
              >
               Saiba mais
              </button>
            </div>
            <a
              className="uppercase border-l-4 border-blue-500 p-2  text-xs font-bold tracking-wider hidden md:block"
              href="#"
            >
             Saiba mais
            </a>
            <article className="h-0 md:h-auto -mt-4 md:mt-0 overflow-hidden">
              <ul className="my-5 text-sm tracking-wide">
                <li className="my-3 tracking-wide">
                  <a href="https://www.ibm.com/br-pt/products/?lnk=fdi">Produtos</a>
                </li>
                <li className="my-3 tracking-wide">
                  <a href="https://www.ibm.com/br-pt/products/trials?lnk=fdi_brpt">Testes de Software</a>
                </li>
                <li className="my-3 tracking-wide">
                  <a href="https://www.ibm.com/br-pt/services?lnk=fdi">Serviços</a>
                </li>
                <li className="my-3 tracking-wide">
                  <a href="https://www.ibm.com/br-pt/industries?lnk=fdi_brpt">Indústria</a>
                </li>
              </ul>
            </article>
          </section>
         
          <section
            className="relative text-gray-700 font-light font-light border-b px-6 pb-4 md:py-3 w-full md:border-none md:w-1/4"
          >
            <div className="md:hidden">
              <button
                   onClick={() => toggleFooterSection(event)}
                className="uppercase text-xs font-bold tracking-wider focus:outline-none border-t border-white py-4 w-full text-left"
                type="button"
              >
                Informações para...
              </button>
            </div>
            <a
              className="uppercase text-xs border-l-4 border-blue-500 p-2  font-bold tracking-wider hidden md:block"
              href="#"
            >
              Informações para...
            </a>
            <article className="h-0 md:h-auto -mt-4 md:mt-0 overflow-hidden">
              <ul className="my-5 text-sm tracking-wide">
                <li className="my-3 tracking-wide">
                  <a href="https://developer.ibm.com/br/">Desenvolvedores</a>
                </li>
                <li className="my-3 tracking-wide">
                  <a href="https://www.ibm.com/br-pt/products/partneratibm?lnk=hmmpr_brpt&lnk2=learn">Parceiros Comerciais</a>
                </li>
              </ul>
            </article>
          </section>

          <section
            className="relative text-gray-700 font-light font-light border-b px-6 pb-4 md:py-3 w-full md:border-none md:w-1/4"
          >
            <div className="md:hidden">
              <button
                   onClick={() => toggleFooterSection(event)}
                className="uppercase text-xs font-bold tracking-wider focus:outline-none border-t border-white py-4 w-full text-left"
                type="button"
              >
                Links
              </button>
            </div>
            <a
              className="uppercase text-xs border-l-4 border-blue-500 p-2  font-bold tracking-wider hidden md:block"
              href="#"
            >
              Links
            </a>
            <article className="h-0 md:h-auto -mt-4 md:mt-0 overflow-hidden">
              <ul className="my-5 text-sm tracking-wide">
                <li className="my-3 tracking-wide">
                  <a href="https://www.linkedin.com/company/ibm/">Linkedin</a>
                </li>
                <li className="my-3 tracking-wide">
                  <a href="https://www.instagram.com/ibm/">Instagram</a>
                </li>
                <li className="my-3 tracking-wide">
                  <a href="https://twitter.com/ibmbrasil?ref_src=twsrc%5Egoogle%7Ctwcamp%5Eserp%7Ctwgr%5Eauthor">Twitter</a>
                </li>
              </ul>
            </article>
          </section>
        
         
        </div>
      </div>
      <div className="bg-currentDark  px-4 text-white">
        <section
          className="max-w-screen-lg mx-auto flex flex-col md:flex-row md:justify-between text-gray-700 font-light text-sm pt-4 pb-6 md:pt-5 md:pb-6 w-full">
          <div>
            <p className="leading-8 tracking-wide text-white">
              &copy; IBM - Av. Santos Dumont, 1789 - Aldeota, Fortaleza - CE, 60150-160
            </p>
          </div>
          <div>
            <p className="leading-8 tracking-wide text-white ">Politicas de privacidade / Termos de uso</p>
          </div>
        </section>
      </div>
    </footer>
    
     
  
)

export default Footer
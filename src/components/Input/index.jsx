const Input = ({error, children, disabled, label, value, placeholder, name, type, refs, defaultValues}) => {
 
      return (
        <div className="my-3">
            <label htmlFor={label} className="block text-xs font-semibold text-white-600 uppercase">{label}</label>
            <input defaultValue={defaultValues} ref={refs} id={label} type={type} name={name} placeholder={placeholder}
                className={`block w-full py-3 px-2 my-1
                text-sm
                appearance-none 
                rounded	
                border-2 
                ${error || "border-white-200"}
                focus:outline-none`}/>
            {children}
        </div>
      )
 
}

export default Input
import Header from "../Header";
import Footer from "../Footer";

const Container = ({children}) => (
    <div className="page bg-white">
    <Header title="My tasks" color="bg-current" />
        <div className="w-full md:max-w-6xl 2xl:max-w-7xl mx-auto md:my-5">
            {children}
        </div>
    <Footer />
  </div>
  )
  
  export default Container

import { Transition } from "@headlessui/react";
import Link from "next/link";
import { useState, useEffect } from "react";
import { signIn, signOut, useSession } from "next-auth/client";
import api from "../../services/api";
import {
  FiLogOut
} from "react-icons/fi";
import router, { useRouter } from "next/router";


const Menu = ({ title, color, socket }) => {
  const [session] = useSession();
  const [query, setQuery] = useState("");

  return (
    <header>
      <div className={`relative ${color}`}>
        <div className="max-w-7xl mx-auto px-4 sm:px-6">
          <div className="flex w-full  justify-between items-center py-2">
            <div className="flex text-white justify-start">
              <Link href="/">
                <a className="-m-4 p-3 flex items-start rounded-lg">
                <img className="items-center" alt="negocio fechado" width="110" src="https://upload.wikimedia.org/wikipedia/commons/thumb/5/51/IBM_logo.svg/270px-IBM_logo.svg.png" />
                </a>
              </Link>
            </div>
           
            {session ? (
              <div className="flex">
                <div className="flex text-white mr-11 group focus:outline-none rounded-md  items-center text-base font-medium ">
                  {session.user.name}
                </div>
                <button onClick={() => signOut()}>
                  <a className="p-3 flex items-start rounded-lg">
                    <div className="mr-2">
                      <p className="text-base font-medium text-white">
                        Sair
                      </p>
                    </div>
                    <FiLogOut size="22" color="white" />
                  </a>
                </button>

               
                                  
              </div>
            ) : (
              <>
                <div className="hidden md:flex items-center justify-end">
                  {!session && (
                    <>
                      <button
                        className="whitespace-nowrap text-base font-medium text-white	hover:text-gray-900"
                        onClick={() => signIn()}
                      >
                        Entrar
                      </button>
                    </>
                  )}
                </div>
              </>
            )}
          </div>
        </div>
      </div>
    </header>
  );
};

export default Menu;

import React, { useState, useEffect, useRef } from "react";
import Link  from "next/link";
import Container from "../../components/Container"
import api from "../../services/api";
import { useForm, Controller } from "react-hook-form";
import Heading, { HeadingLine } from "../../components/Heading";
import Button from "../../components/Button";
import { getSession } from "next-auth/client";
import Input from "../../components/Input";
import Router from 'next/router'
import swal from "sweetalert";
const NovaTarefa = ({session}) => {


  const {
    register,
    watch,
    handleSubmit,
    control,
    setValue,
    errors,
  } = useForm();

  const onSubmit = (data) => {
      api
        .post("/tarefa",{usuario_id: session.user.id, descricao: data.descricao, data_ini: `${data.dt_ini} ${data.hr_ini}`, data_fim: `${data.dt_fim} ${data.hr_fim}`})
        .then((response) => {
          if (response.data.res) {
            swal(
              "Sucesso!",
              "Foi adicionado uma nova tarefa no seu calendário na página inicial",
              "success"
            ).then((value) => {
              Router.push('/')
            });
          } else {
            swal("Algo deu errado!", response.data.res, "error");
          }
        });
    console.log(data);
  };

    return (
        <>
        <Container>
          
        <div className="flex w-full justify-center items-center py-10 rounded-sm shadow-sm flex-col mt-10 h-full lg:bg-default sm:bg-white">
          <div className="mx-2 ">
            <div
              className="w-full
               sm:py-6 
                bg-white rounded-lg"
            >
              <div className="-mx-1 uppercase text-xs items-center flex justify-between">
                <HeadingLine
                  size="text-xl "
                  lineColor="border-blue-500"
                >
                  Nova Tarefa
                </HeadingLine>
              </div>

              <div className="flex w-full flex-col-reverse md:flex-row">
                <form
                  autoComplete="off"
                  onSubmit={handleSubmit(onSubmit)}
                  className="mt-2"
                  method="POST"
                >
                  <div className="-mx-3 -mb-2 md:flex">
                    <div className="md:w-full px-2">
                      <Input
                        refs={register({
                          required: true,
                        })}
                        label="Descrição"
                        placeholder="Descrição"
                        name="descricao"
                        type="text"
                      >
                      </Input>
                    </div>
                  </div>


                  <div className="-mx-3 -mb-2 md:flex">
                    <div className="md:w-7/12 px-2">
                      <Input
                        refs={register({
                          required: true,
                        })}
                        label="Data de inicio"
                        placeholder="Data de inicio"
                        name="dt_ini"
                        type="date"
                      >
                      </Input>
                    </div>
                    <div className="md:w-5/12 px-2">
                      <Input
                        refs={register({
                          required: true,
                        })}
                        label="Horário"
                        placeholder="Horário"
                        name="hr_ini"
                        type="time"
                      >
                      </Input>
                    </div>
                  </div>



                  <div className="-mx-3 -mb-2 md:flex">
                    <div className="md:w-7/12 px-2">
                      <Input
                        refs={register({
                          required: true,
                        })}
                        label="Data de Término"
                        placeholder="Data de Término"
                        name="dt_fim"
                        type="date"
                      >
                      </Input>
                    </div>
                    <div className="md:w-5/12 px-2">
                      <Input
                        refs={register({
                          required: true,
                        })}
                        label="Horário"
                        placeholder="Horário"
                        name="hr_fim"
                        type="time"
                      >
                      </Input>
                    </div>
                  </div>

                  <div className="-mx-1 md:flex flex  md:justify-end mt-3">
                    <Button size="w-full" color="bg-blue-500" hoverColor="bg-blue-600">
                      Cadastrar
                    </Button>
                  </div>
                </form>

              </div>
            </div>
          </div>
        </div>
        </Container>

        </>
    );
}

NovaTarefa.getInitialProps = async (ctx) => {
    const { req, res } = ctx;
    const session = await getSession({ req });
    if (session) {
      return {session};
    } else {
        res.writeHead(302, {
            Location: "/",
          });
          res.end();
          return;
    }
  
  }
  

    
export default NovaTarefa;



import { NextApiHandler } from "next";
import NextAuth from "next-auth";
import Providers from "next-auth/providers";
import api from '../../../services/api';

export default NextAuth({
  providers: [
    Providers.Credentials({
      name: "Credentials",
      credentials: {
        usuario: { label: "usuario", type: "text", placeholder: "Usuario" },
        senha: { label: "senha", type: "senha" },
      },
      authorize: async (credentials) => {
        try {
        const user = await api.post("login",{email:credentials.username,senha: credentials.password});

        console.log("esse =>" ,user.data[0])

        if(user.data[0]){
          return Promise.resolve({
            name: user.data[0].nome,
            email: user.data[0].email,
            id:user.data[0].id,
          });
        }else{
          return Promise.resolve(null);
        }
         
        } catch (error) {
          return Promise.resolve(null);
        }
        
      },
    }),
  ],
  session: {
    jwt: true,
    maxAge: 1 * 24 * 60 * 60, // 7 days
  },
  pages: {
    signIn: "/login",
  },
  callbacks: {

    jwt: async (token, user, account, profile, isNewUser) => {
     
      user && (token.user = user);
      return Promise.resolve(token)   // ...here
  },
 
    session: async (session, user, sessionToken) => {
      //  "session" is current session object
      //  below we set "user" param of "session" to value received from "jwt" callback
      session.user = user.user;
      return Promise.resolve(session)
  }
  }
  
});

import {useEffect, useRef} from "react";
import Head from "next/head";
import Router from 'next/router'
import "tailwindcss/tailwind.css";
import { Provider } from 'next-auth/client'
import "../styles/global.css";
import io from "socket.io-client";
import {toast } from "react-toastify";
import { useSession } from "next-auth/client";
import NProgress from 'nprogress'

Router.events.on('routeChangeStart', (url) => {
  console.log(`Loading: ${url}`)
  NProgress.start()
})
Router.events.on('routeChangeComplete', () => NProgress.done())
Router.events.on('routeChangeError', () => NProgress.done())


const App = ({ Component, pageProps }) => {
  const [session, loading] = useSession();


  const { NEXT_PUBLIC_APP_IP } = process.env;
  const socketRef = useRef();
  socketRef.current = io(NEXT_PUBLIC_APP_IP);

  useEffect(() => {
    if (session) {
      socketRef.current.emit("join", userInfo().fornecedor_id);
      socketRef.current.emit("user", userInfo().usuario_id);
    }

     if (session) {
       socketRef.current.on("receive_notification", (data) => {
        openNotification(data);
      });
    }

   
  }, []);

  const openNotification = (data) => {
    console.log(data)
   if (data.notification.tipo === 1) {
     toast.info(`${data.notification.msg}`, {
       position: "bottom-right",
       autoClose: 5000,
       hideProgressBar: false,
       closeOnClick: true,
       pauseOnHover: true,
       draggable: true,
       progress: undefined,
     })
   } else if(data.notification.tipo === 2) {
     toast.info(`${data.notification.msg}`, {
       position: "bottom-right",
       autoClose: 5000,
       hideProgressBar: false,
       closeOnClick: true,
       pauseOnHover: true,
       draggable: true,
       progress: undefined,
     })
   }else{
     toast.info(`${data.notification.msg}`, {
       position: "bottom-right",
       autoClose: 5000,
       hideProgressBar: false,
       closeOnClick: true,
       pauseOnHover: true,
       draggable: true,
       progress: undefined,
     })
   }
 };
  return (
    <Provider session={pageProps.session}>
        <Head>
          <title>My Task</title>
          <link rel="icon" href="/favicon.ico" />
          <link rel="manifest" href="/manifest.json" />
          <link rel="stylesheet" type="text/css" href="/nprogress.css" />
          <meta
            name="description"
            content="My Task"
          />
          </Head>
        <Component socket={socketRef.current} {...pageProps} />
    </Provider>
   
  );
};

export default App;

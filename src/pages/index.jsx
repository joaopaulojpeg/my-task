import React, { Component, useState, useEffect } from "react";
// import BigCalendar from 'react-big-calendar'
import moment from 'moment'
import { Calendar, momentLocalizer  } from 'react-big-calendar' 
import 'react-big-calendar/lib/css/react-big-calendar.css';
import SideModal from "../components/SideModal";
import Heading, { HeadingLine } from "../components/Heading";
import { useForm, Controller } from "react-hook-form";
import Input from "../components/Input";
import Button, { ButtonLink } from "../components/Button";
import api from "../services/api";
import {getSession} from "next-auth/client";
import swal from "sweetalert";

import Container from "../components/Container"
import { GiWhiteBook } from "react-icons/gi";
moment.locale('pt-BR');
const localizer = momentLocalizer(moment)


// The component you should use instead the one you mentioned.

// const allViews = Object
//   .keys(Calendar.Views)
//   .map(k => Calendar.Views[k])
function eventStyleGetter(event, start, end, isSelected) {
  console.log(event);
  var backgroundColor;
  (event.status == 1) ? backgroundColor = '#F59E0B' : ((event.status == 2) ? backgroundColor = '#22C55E' :  backgroundColor = '#F43F5E') 
  

  var style = {
      className:'bg-red-500' ,
      backgroundColor: backgroundColor,
      color: "#fff",
      borderRadius: '0px',
      opacity: 0.8,
      border: '0px',
      display: 'block'
  };
  return {
      style: style
  };
}
  const events = [
    {
      'id': 1,
      'title': 'Criar documentação',
      'allDay': false,
      'start': '2021-07-10 14:00',
      'end': '2021-07-10 23:59'
    },
    {
      'id': 2,
      'title': 'Conversar com RH',
      'allDay': false,
      'start': '2021-07-10 00:00',
      'end': '2021-07-10 23:59'
    },
    {
      'id': 3,
      'title': 'Conversar com RH2',
      'allDay': false,
      'start': '2021-07-11 00:00',
      'end': '2021-07-11 10:00',
    },
]

const Home = ({session}) => {
  const {
    register,
    watch,
    handleSubmit,
    control,
    setValue,
    errors,
  } = useForm();
  const [showEdit, setShowEdit] = useState(false);
  const [task_id, setTaskId] = useState(null);

  const [tarefas, setTarefas] = useState([]);

  useEffect(() => {
    api
      .get("/tarefa/"+session.user.id)
      .then((response) => {
        setTarefas(response.data);
      });

  }, []);

  console.log(tarefas)
  
  const openModalEdit = (event) => {
    setTaskId(event.id)
    setShowEdit(true)
    setTimeout(function () {
      console.log(event)
      setValue("id", event.id);
      setValue("descricao", event.title);
      setValue("dt_ini", moment(event.start).format('YYYY-MM-DD'));
      setValue("hr_ini", moment(event.start).format('HH:mm'));
      setValue("dt_fim", moment(event.end).format('YYYY-MM-DD'));
      setValue("hr_fim", moment(event.end).format('HH:mm'));

    }, 100);
  }
  const onSubmit = (data) => {
    api
  .put("/tarefa/"+task_id,{descricao: data.descricao, dt_ini: `${data.dt_ini} ${data.hr_ini}`, dt_fim: `${data.dt_fim} ${data.hr_fim}` })
    .then((response) => {
      console.log(response)
      swal("Sucesso!", "A tarefa foi editada", "success").then(() => window.location.reload(true));
    });
  }
  const removerTarefa = () => {
    console.log(task_id)
    api
    .delete("/tarefa",{data:{tarefa_id: task_id}})
      .then((response) => {
        swal("Sucesso!", "A tarefa foi removida", "success").then(() => window.location.reload(true));
      })
      .catch(err => alert(err.stack))
  }
  const concluirTarefa = () => {
    console.log(task_id)
    api
    .post("/tarefaConcluida",{tarefa_id: task_id})
      .then((response) => {
        swal("Sucesso!", "A tarefa foi concluída", "success").then(() => window.location.reload(true));
      })
      .catch(err => alert(err.stack))
  }
  const renderModalEditEvent = () => {
    return(
      <>
        <SideModal show={showEdit} onClose={() => setShowEdit(false)}>
          <div className="mx-2 ">
            <div
              className="lg:w-full
                px-6 py-10 sm:px-10 sm:py-6 
                rounded-lg"
            >
              <div className="-mx-1 uppercase text-xs items-center flex justify-between">
                <HeadingLine
                  size="text-xl "
                  lineColor="border-blue-500"
                >
                  Editar Atividade
                </HeadingLine>
                <div className="bg-green-400 text-white p-3 cursor-pointer " onClick={concluirTarefa}>Concluída</div>
                <div className="cursor-pointer text-red-400 ml-2" onClick={removerTarefa}>Excuir</div>
              </div>

              <div className="flex w-full flex-col-reverse md:flex-row">
                <form
                  autoComplete="off"
                  onSubmit={handleSubmit(onSubmit)}
                  className="mt-2 w-full"
                  method="POST"
                >
                  <input type="hidden" name="id" />

                  <div className="md:w-full px-2">
                    <Input
                      refs={register({
                        required: true,
                      })}
                      error={errors.nome && "border-red-500"}
                      label="Descrição da Atividade"
                      placeholder="Descrição da Atividade"
                      name="descricao"
                      type="text"
                    >
                      {errors.nome && (
                        <span className="text-sm text-red-500">
                          Este campo é obrigatório
                        </span>
                      )}
                    </Input>
                  </div>

                  <div className="md:w-full px-2">
                    <Input
                      refs={register({
                        required: true,
                      })}
                      error={errors.nome && "border-red-500"}
                      label="Data de Início da atividade"
                      placeholder="Data de Início da atividade"
                      name="dt_ini"
                      type="date"
                    >
                      {errors.nome && (
                        <span className="text-sm text-red-500">
                          Este campo é obrigatório
                        </span>
                      )}
                    </Input>
                  </div>

                  
                  <div className="md:w-full px-2">
                    <Input
                      refs={register({
                        required: true,
                      })}
                      error={errors.nome && "border-red-500"}
                      label="Horário de Início da atividade"
                      placeholder="Horário de Início da atividade"
                      name="hr_ini"
                      type="time"
                    >
                      {errors.nome && (
                        <span className="text-sm text-red-500">
                          Este campo é obrigatório
                        </span>
                      )}
                    </Input>
                  </div>

                  <div className="md:w-full px-2">
                    <Input
                      refs={register({
                        required: true,
                      })}
                      error={errors.nome && "border-red-500"}
                      label="Data de Término da atividade"
                      placeholder="Data de Término da atividade"
                      name="dt_fim"
                      type="date"
                    >
                      {errors.nome && (
                        <span className="text-sm text-red-500">
                          Este campo é obrigatório
                        </span>
                      )}
                    </Input>
                  </div>


                  <div className="md:w-full px-2">
                    <Input
                      refs={register({
                        required: true,
                      })}
                      error={errors.nome && "border-red-500"}
                      label="Horário de Término da atividade"
                      placeholder="Horário de Término da atividade"
                      name="hr_fim"
                      type="time"
                    >
                      {errors.nome && (
                        <span className="text-sm text-red-500">
                          Este campo é obrigatório
                        </span>
                      )}
                    </Input>
                  </div>

                  <div className="md:flex md:justify-end mt-5">
                    <Button size="w-full" color="bg-blue-500" hoverColor="bg-blue-600">
                      Salvar
                    </Button>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </SideModal>

      </>
    )
  }

  return (
    <Container>
      {renderModalEditEvent()}
      <div className="bg-white rounded px-3 flex justify-between items-center my-2">
        <HeadingLine size="text-xl" lineColor="border-blue-500">
          Minhas Tarefas
        </HeadingLine>

        <ButtonLink link="adicionar-tarefa" size="w-46" textSize="text-md" color="bg-blue-500" hoverColor="bg-blue-600">
          Nova Atividade
        </ButtonLink>
      </div>
      <div className="flex my-5 px-4 items-center">
        <div className="mx-1 bg-green-500 rounded-full p-1 px-5 text-white">Concluído</div>
        <div className="mx-1 bg-yellow-500 rounded-full p-1 px-3 text-white">Pendente</div>
        <div className="mx-1 bg-red-500 rounded-full p-1 px-3 text-white">Atrasados</div>
      </div>
      <div className="flex md:flex-row flex-col-reverse">
      <div className="h-1/3 md:w-8/12 w-11/12 md:mr-10  content-center m-3 md:visible sm:invisible calendar">
        <Calendar
          selectable={false}
          events={tarefas}
          views={["month"]}
          startAccessor="start"
          endAccessor="end"
          defaultDate={moment().toDate()}
          localizer={localizer}
          onSelectEvent={event => openModalEdit(event)}
          eventPropGetter={event => eventStyleGetter(event, event.start, event.end, event.isSelected)}
        />
     </div>
      <div className="flex-1 flex-col w-full">
        {tarefas.map((item) => (
        <div onClick={() => openModalEdit(item)} className="w-full cursor-pointer shadow-md hover:shadow-lg hover:bg-gray-100 rounded-lg bg-white m-1 ">
          <div className="p-1 px-2 flex flex-col  w-full">
      
            <p className="text-base text-gray-600 text-lg my-2 uppercase">
              <HeadingLine
                  size="text-md"
                  colorBorder={(item.status == 1) ? "#eab308" : ((item.status == 2) ? "#22c55e" : "red") }
                >
                  <div className="flex flex-row-reverse content-center text-sm w-full">
                  {(item.status == 3) ? <span class="flex h-3 w-3">
                    <span class="m-x-5 animate-ping absolute inline-flex h-3 w-3 rounded-full bg-red-400 opacity-75"></span>
                    <span class="m-x-5 relative inline-flex rounded-full h-3 w-3 bg-red-500"></span>
                  </span> : ''}
                  
                    {moment(item.start).format('DD/MM/YYYY HH:mm')} ~ {moment(item.end).format('DD/MM/YYYY HH:mm')}
                  </div>
                  {item.title}
              </HeadingLine>
            </p>
          </div>
        </div>
        ))}
      </div>

      
    </div>
    </Container>

  );
}

Home.getInitialProps = async (ctx) => {
  const { req, res } = ctx;
  const session = await getSession({ req });

  if (session) {
    return {session:session};
  }else{
    res.writeHead(302, {
      Location: "/login",
    });
    res.end();
    return;
  }

}
export default Home;

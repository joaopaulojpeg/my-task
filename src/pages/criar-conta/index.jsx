import React, { useState, useEffect, useRef } from "react";
import Input, { InputMask, Select } from "../../components/Input";
import Button from "../../components/Button";
import Heading, { HeadingLine } from "../../components/Heading";
import axios from "axios";
import Mask from "react-input-mask";
import { isValidCPF } from "../../functions";
import Link from "next/link";
import { useForm, Controller } from "react-hook-form";
import MaskedInput from "react-input-mask";
import { IoIosArrowForward } from "react-icons/io";
import swal from "sweetalert";
import api from "../../services/api";

export default function Registration({ history }) {
  const {
    register,
    handleSubmit,
    watch,
    setValue,
    errors,
    control,
  } = useForm();

  const onSubmit = (data) => {

    api.post("/insertLogin", data).then((response) => {
      if (response) {
        swal(
          "Sucesso!",
          `Cadastro realizado com sucesso`,
          "success"
        ).then((value) => {
          window.location.href = "/login"
        });
      } else {
        swal("Algo deu errado!", "verifique os dados e tente novamente", "error");
      }
    });
  };

  return (
    <>
      <div className="flex flex-col mt-5 h-screen lg:bg-default sm:bg-white">
        <div className="grid place-items-center mx-2 my-30">
          <div
            className="w-11/12 p-12 lg:w-7/12 
                px-6 py-10 sm:px-10 sm:py-6 
                bg-white rounded-lg"
          >
            <div className="-mx-1  text-xs items-center flex justify-between">
              <HeadingLine size="text-2xl" lineColor="border-blue-400" weight="font-thin">
                Criar conta
              </HeadingLine>
            </div>
            <form
              autoComplete="off"
              onSubmit={handleSubmit(onSubmit)}
              className="mt-2"
              method="POST"
            >
                <div className="md:w-full px-2">
                  <Input
                    refs={register({ required: true })}
                    error={errors.nome && "border-red-500"}
                    label="Nome"
                    placeholder="Nome"
                    name="nome"
                    type="text"
                  >
                    {errors.nome && (
                      <span className="text-sm text-red-500">
                        Este campo é obrigatório
                      </span>
                    )}
                  </Input>
                </div>
                <div className="md:w-full px-2">
                  <Input
                    refs={register({
                      required: true,
                    })}
                    error={errors.usuario && "border-red-500"}
                    label="Email"
                    placeholder="Email"
                    name="usuario"
                    type="email"
                  >
                    {errors.usuario && errors.usuario.type === "required" && (
                      <span className="text-sm text-red-500">
                        Este campo é obrigatório
                      </span>
                    )}
                  </Input>
                </div>

                <div className="md:w-full px-2">
                  <Input
                    refs={register({ required: true })}
                    error={errors.senha && "border-red-500"}
                    label="Senha"
                    placeholder="Senha"
                    name="senha"
                    type="password"
                  >
                    {errors.password && (
                      <span className="text-sm text-red-500">
                        Este campo é obrigatório
                      </span>
                    )}
                  </Input>
                </div>
                
              <div className="md:flex md:justify-end mt-5 px-2">
                <Button size="w-full md:w-60" color="bg-blue-500" hoverColor="bg-blue-600">
                  Enviar
                </Button>
              </div>
            </form>
          </div>
        </div>
      </div>
    </>
  );
}

import { FaWindowRestore } from "react-icons/fa";

export const AUTENTICATE = "@autenticado";
export const INFO = "@userInfo";

export const isAuthenticated =  () => localStorage.getItem(AUTENTICATE);
export const userInfo = () => JSON.parse(localStorage.getItem(INFO));


export const getToken = () =>  localStorage.getItem(AUTENTICATE);

//  export const login = async token => {

//   localStorage.setItem(INFO, JSON.stringify(await token.permissao[0]));
//   localStorage.setItem(AUTENTICATE,await token.res);

//   console.log(isAuthenticated());
// };

export const login = token => {
    localStorage.setItem(INFO, JSON.stringify(token.data[0]));
    localStorage.setItem(AUTENTICATE,token.res);

    console.log(token)
   // console.log(isAuthenticated());
}





export const logout = () => {
  localStorage.removeItem(AUTENTICATE);
  localStorage.removeItem(INFO);


};
import axios from 'axios';
// const { REACT_APP_IP , REACT_APP_IP_WITH_PORT} = process.env;

console.log(process.env.NEXT_PUBLIC_APP_IP_WITH_PORT)

const api = axios.create({
    baseURL: process.env.NEXT_PUBLIC_APP_IP
});

export const service = axios.create({
    baseURL: process.env.NEXT_PUBLIC_APP_IP_WITH_PORT
});

export default api;
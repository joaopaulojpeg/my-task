const colors = require('tailwindcss/colors')


module.exports = {
  purge: ['./src/pages/**/*.{js,ts,jsx,tsx}', './src/components/**/*.{js,ts,jsx,tsx}'],
  darkMode: false, // or 'media' or 'class'
  theme: {
    colors: {
      transparent: 'transparent',
      current: '#2F354A',
      currentDark: '#222736',
      orange:"#F7901E",
      laranja: '#CC5930',
      azuldark: '#222736',
      verde: '#A7F3D0',
      azulclaro: '#43AAB1',
      roxo: '#945CA3',
      reprovar: '#340D1A',
      repcliente: '#F7EEB7',
      backgroundWhite: '#ecf1f8',
      black: colors.black,
      white: colors.white,
      gray: colors.trueGray,
      indigo: colors.indigo,
      red: colors.rose,
      yellow: colors.amber,
      emerald: colors.emerald,
      green: colors.green,
      orange: colors.orange,
      blue: colors.blue,
      pink: colors.pink,
      purple: colors.purple
    },
    
  },
  variants: {
  },
  plugins: [
    
  ],
  
}
